package tw.teddysoft.bdd.specification.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class CreateTeamSpec {
    static Feature feature;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Team Use Case", """
                    A team is created asynchronously after a user registers successfully for ezKanban.                
                    The user becomes the admin of the team (i.e., having the team admin role).
                """);
    }

    @EzScenario
    public void If_a_user_registration_is_completed_a_team_belonging_to_the_user_should_be_created() {
        feature.newScenario()
                .Given("a new user named ${userName=Teddy} with user id ${userId=t001}", env -> {
                    pending();
                })
                .And("he has successfully registered for Miro", env -> {
                    pending();
                })
                .When("the registration process is completed, which triggers the automatic creation of a team for Teddy", env -> {
                    pending();
                })
                .Then("a team should be created for Teddy", env -> {
                    pending();
                })
                .And("Teddy should be designated as the team admin", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void If_a_user_registration_is_completed_a_team_belonging_to_the_user_should_be_created_zh_tw() {
        feature.newScenario()
                .Given("一個使用者 $Teddy，他的使用者號碼是 $t001", env -> {
                    pending();
                })
                .And("他成功註冊Miro", env -> {
                    pending();
                })
                .When("他的註冊流程完畢,驅動系統自動幫他建立一個Team", env -> {
                    pending();
                })
                .Then("一個屬於Teddy的團隊應該被建立", env -> {
                    pending();
                })
                .And("Teddy應該被指派為team admin", env -> {
                    pending();
                })
                .Execute();
    }

}
