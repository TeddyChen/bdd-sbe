package tw.teddysoft.bdd.specification.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class UnstarBoardSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Unstar Board Use Case", """
                You can unstart boards that are no longer used frequently so that they are removed from the starred project.
                """);
    }

    @EzScenario
    public void Unstaring_a_starred_board_should_remove_it_from_the_starred_project() {
        feature.newScenario()
                .Given("a registered user ${userName=Teddy} with ${userId=teddy_001}", env -> {
                    pending();
                })
                .And("an starred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("Teddy unstar the board", env -> {
                    pending();
                })
                .Then("the board does not exist in the starred project", env -> {
                    pending();
                })
                .And("the board is still in the default project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Unstaring_an_unstarred_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user ${userName=Teddy} with ${userId=teddy_001}", env -> {
                    pending();
                })
                .And("an unstarred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("Teddy unstar the board", env -> {
                    pending();
                })
                .Then("the board does not change its state", env -> {
                    pending();
                })
                .Execute();
    }



}
