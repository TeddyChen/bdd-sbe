package tw.teddysoft.bdd.specification.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class StarBoardSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Star Board Use Case", """
                You can start boards that are frequently used so that they can be quickly found in the starred project.
                """);
    }

    @EzScenario
    public void Staring_an_unstarred_board_should_add_it_to_the_starred_project() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an unstarred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("he stars the board", env -> {
                    pending();
                })
                .Then("the board should be found in the starred project", env -> {
                    pending();
                })
                .And("the board should be still presented in the default project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Staring_a_starred_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an starred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("he stars the board", env -> {
                    pending();
                })
                .Then("the board should not change its state", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Staring_a_non_existing_board_should_fail() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .When("he stars a non-existing $Scrum board", env -> {
                    pending();
                })
                .Then("the operation should fail", env -> {
                    pending();
                })
                .And("he should be notified that the starred board does not exist", env -> {
                })
                .Execute();
    }

}
