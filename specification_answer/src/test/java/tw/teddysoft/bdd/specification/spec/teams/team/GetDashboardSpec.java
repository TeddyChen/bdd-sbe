package tw.teddysoft.bdd.specification.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static tw.teddysoft.ezspec.exception.PendingException.pending;


@EzFeature
@EzFeatureReport
public class GetDashboardSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Get Dashboard Use Case", """
                    This is a query to get the view model for clients to render UI for the dashboard.               
                """);
    }

    @EzScenario
    public void If_Teddy_logged_in_successfully_he_should_see_a_personal_dashboard() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("two user defined projects: $DDD and $CA", env -> {
                    pending();
                })
                .And("a $Scrum board in the default project", env -> {
                    pending();
                })
                .And("a $Kanban board and a $Pattern board in the $DDD project", env -> {
                    pending();
                })
                .And("the $Scrum board has been starred", env -> {
                    pending();
                })
                .And("the $Pattern board has been trashed", env -> {
                    pending();
                })
                .When("he logs in Miro", env -> {
                    pending();
                })
                .Then("he should see a dashboard containing two user defined projects", env -> {
                    pending();
                })
                .And("the $Kanban board should be in the $DDD project", env -> {
                    pending();
                })
                .And("the $Pattern board should not exist in the $DDD project", env -> {
                    pending();
                })
                .And("the $Pattern board should be in the trash project", env -> {
                    pending();

                })
                .And("the $Scrum board should be in both the default and the starred projects", env -> {
                    pending();
                })
                .And("the team should contain a team admin", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void If_Teddy_logged_in_successfully_he_should_see_a_personal_dashboard_table_arguments_version() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("the following user defined projects:\n" + """
                        | user_defined_project |
                        | DDD                  |
                        | CA                   |""",    env ->{

                    pending();
                })
                .And("the following boards:\n" + """
                        | board_name    |  in_project   |   starred |   trashed |
                        | Scrum         |   DEFAULT     |   true    |   false   |
                        | Kanban        |   DDD         |   false   |   false   |
                        | Pattern       |   DDD         |   false   |   true    |
                        """, env -> {

                    pending();
                })
                .When("he logs in Miro", env -> {
                    pending();
                })
                .Then("he should see a dashboard as followings:\n" + """
                        | board_name    |   in_default |   in_ddd_project     |  in_ca_project    |   in_starred_project |  in_trashed_project |
                        | Scrum         |   true       |   false              |  false            |       true           |      false          |
                        | Kanban        |   false      |   true               |  false            |       false          |      false          | 
                        | Pattern       |   false      |   false              |  false            |       false          |      true           |
                        """, env -> {

                    pending();
                })
                .And("the team should contain a team admin Teddy", env -> {
                    pending();
                })
                .Execute();
    }

}
