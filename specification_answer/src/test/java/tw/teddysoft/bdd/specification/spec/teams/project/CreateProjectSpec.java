package tw.teddysoft.bdd.specification.spec.teams.project;

import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class CreateProjectSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Project Use Case", """
                Users create projects to categorize their boards.
                The project name cannot be duplicated.
                Projects can be created directly under the user's team.
                That is, nested project structures are not supported.
                """);
    }
    @EzScenario
    public void Creating_a_project_with_unique_name() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .When("he creates a project named $SBE", env -> {
                    pending();
                })
                .Then("the team should contain a new project named SBE", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Cannot_create_a_project_with_duplicated_project_named() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a $DDD project in his team", env ->{
                    pending();
                })
                .When("he creates a project named $DDD", env -> {
                    pending();
                })
                .Then("the second DDD project cannot be created due to project name duplication", env -> {
                    pending();
                })
                .Execute();
    }

}
