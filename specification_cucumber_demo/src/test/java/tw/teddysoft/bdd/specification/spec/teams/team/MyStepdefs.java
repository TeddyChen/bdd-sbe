package tw.teddysoft.bdd.specification.spec.teams.team;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MyStepdefs {
    @Given("一個使用者 $Teddy，他的使用者號碼是 $t{int}")
    public void 一個使用者$Teddy他的使用者號碼是$t(int arg0) {
    }

    @And("他成功註冊Miro")
    public void 他成功註冊miro() {
    }

    @When("他的註冊流程完畢,驅動系統自動幫他建立一個Team")
    public void 他的註冊流程完畢驅動系統自動幫他建立一個Team() {
    }

    @Then("一個屬於Teddy的團隊應該被建立")
    public void 一個屬於teddy的團隊應該被建立() {
    }

    @And("Teddy應該被指派為team admin")
    public void teddy應該被指派為teamAdmin() {
    }

    @Given("a new user named $\\{userName=Teddy} with user id $\\{userId=t{int}}")
    public void aNewUserNamed$UserNameTeddyWithUserId$UserIdT(int arg0) {
    }

    @And("he has successfully registered for Miro")
    public void heHasSuccessfullyRegisteredForMiro() {
    }

    @When("the registration process is completed, which triggers the automatic creation of a team for Teddy")
    public void theRegistrationProcessIsCompletedWhichTriggersTheAutomaticCreationOfATeamForTeddy() {
    }

    @Then("a team should be created for Teddy")
    public void aTeamShouldBeCreatedForTeddy() {
    }

    @And("Teddy should be designated as the team admin")
    public void teddyShouldBeDesignatedAsTheTeamAdmin() {
    }
}
