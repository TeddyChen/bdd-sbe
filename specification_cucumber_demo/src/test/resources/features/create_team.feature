Feature: Create Team Use Case

  A team is created asynchronously after a user registers successfully for ezKanban.
  The user becomes the admin of the team (i.e., having the team admin role).


  Scenario: A user registration is completed a team belonging to the user should be created zh tw
    Given 一個使用者 $Teddy，他的使用者號碼是 $t001
    And   他成功註冊Miro
    When 他的註冊流程完畢,驅動系統自動幫他建立一個Team
    Then 一個屬於Teddy的團隊應該被建立
    And Teddy應該被指派為team admin

  Scenario: A user registration is completed a team belonging to the user should be created
    Given a new user named ${userName=Teddy} with user id ${userId=t001}
    And he has successfully registered for Miro
    When the registration process is completed, which triggers the automatic creation of a team for Teddy
    Then a team should be created for Teddy
    And Teddy should be designated as the team admin