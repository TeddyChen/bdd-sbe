package tw.teddysoft.bdd.step_definition_exercise.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class InviteTeamMemberWithBackgroundSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Invite Team Member Use Case", """
                    A team can contain two types of members: Team Admin and Team Staff.                
                    The team creator is a team admin by default.
                    He can invite members as team admins or team staffs.
                """);
    }

    @BeforeEach
    public void background() {
        teamRepository = new InMemoryTeamRepository();

        feature.newBackground()
                .Given("$Teddy is a team admin", env -> {
                    pending();
                }).Execute();
    }

    @EzScenario
    public void Inviting_a_user_a_team_staff() {
        feature.newScenario()
                .When("he invites ${userId=Ada} to his team as a team staff", env -> {
                    pending();
                })
                .Then("$Ada should become a team staff of Teddy's team", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Inviting_a_user_as_a_team_admin() {
        feature.newScenario()
                .When("he invites ${userId=Eiffel} to his team as a team admin", env -> {
                    pending();
                })
                .Then("$Eiffel should become a team admin of Teddy's team", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenarioOutline
    public void Inviting_team_members_of_different_team_roles() {
        String examples = """
                | invited_user_id | team_role |
                | Ada             | Admin     |
                | Eiffel          | Staff     |
                """;

        feature.newScenarioOutline()
                .WithExamples(examples)
                .When("he invites <invited_user_id> to his team as a team <team_role>", env -> {
                    pending();
                })
                .Then("<invited_user_id> should become a team <team_role> of Teddy's team", env -> {
                    pending();
                })
                .Execute();
    }
}
