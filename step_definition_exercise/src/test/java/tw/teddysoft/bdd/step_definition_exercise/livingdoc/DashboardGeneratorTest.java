package tw.teddysoft.bdd.step_definition_exercise.livingdoc;

import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.DashboardGenerator;

import java.io.IOException;

public class DashboardGeneratorTest {

    @Test
    public void generate_dashboard() throws IOException {
        var dashboardGenerator = new DashboardGenerator();

        dashboardGenerator.generate("./target/ezSpec-report");
    }
}
