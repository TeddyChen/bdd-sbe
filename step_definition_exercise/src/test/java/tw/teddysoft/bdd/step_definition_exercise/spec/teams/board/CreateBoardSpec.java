package tw.teddysoft.bdd.step_definition_exercise.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class CreateBoardSpec {
    private TeamRepository teamRepository;
    static Feature feature;
    static final String Rule1_BoardCanOnlyBeCreatedInPhysicalProject = "Boards can only be created in physical projects";
    static final String Rule2_BoardNameCanBeDuplicated = "Board names can be duplicated";
    static final String Rule3_OnlyTeamAdminCanCreateBoard = "Only team admins can creates boards and the creator becomes the board admin";

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Board Use Case", """
                In order to collaborate with my friends 
                As a Miro user
                I want to create a board
                """);
        feature.newRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject);
        feature.newRule(Rule2_BoardNameCanBeDuplicated);
        feature.newRule(Rule3_OnlyTeamAdminCanCreateBoard);
    }
    @BeforeEach
    public void setup(){
        feature.withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject).newBackground("")
                .Given("a registered user ${userName=Teddy}", env -> {
                    pending();
                })
                .Execute();

        feature.withRule(Rule3_OnlyTeamAdminCanCreateBoard).newBackground("")
                .Given("a registered user ${userName=Pascal}", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_without_specifying_its_project_should_create_it_in_the_default_project() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he creates a $Scrum board without specifying a target project", env -> {
                    pending();
                })
                .Then("the board should be created in the default project of the team", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_a_user_defined_project() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .And("a ${projectName=BDD} project belongs to his team", env ->{
                    pending();
                })
                .When("he creates a $Kanban board in the BDD project", env -> {
                    pending();
                })
                .Then("the board should exist in the BDD project", env -> {
                    pending();
                })
                .And("the board should not in the default project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_a_non_existing_user_defined_project_should_fail() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he creates a board in a non-existing BDD project", env -> {
                    pending();
                })
                .Then("the creation should be failed", env -> {
                    pending();
                })
                .And("he should be notified that the BDD project does not exist", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_the_star_should_not_be_allowed() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he wants to create a Scrum board in the star project", env -> {
                    pending();
                })
                .Then("there is no way to do so because boards can be created only be created in physical projects", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule2_BoardNameCanBeDuplicated)
    public void Creating_a_board_with_duplicated_board_name_should_be_allowed() {
        feature.newScenario().withRule(Rule2_BoardNameCanBeDuplicated)
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a $Scrum board in the default project", env -> {
                    pending();
                })
                .When("he creates a board named $Scrum in the default project", env -> {
                    pending();
                })
                .Then("the second $Scrum board should be created in the default project of the team", env -> {
                    pending();
                })
                .And("there should be two boards named $Scrum in the default project", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void Only_team_admins_can_create_a_board_and_becomes_the_board_admin() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .When("he creates a Scrum board in the default project", env -> {
                    pending();
                })
                .Then("the creation should succeed", env -> {
                    pending();
                })
                .And("the board creator Pascal who is the team admin should become the board admin", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void A_user_who_does_not_belong_to_a_team_should_not_be_able_to_create_a_board() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .When("$Ada who does not belong to his team creates a board", env -> {
                    pending();
                })
                .Then("the creation should be failed", env -> {
                    pending();
                })
                .And("$Ada should be notified that users who do not belong to the team cannot create a board", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void A_team_staff_should_not_be_able_to_create_a_board() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .And("$Ada is a team staff of his team", env -> {
                    pending();
                })
                .When("$Ada creates a board named $Scrum in the default project of his team", env -> {
                    pending();
                })
                .Then("the creation should be failed", env -> {
                    pending();
                })
                .And("$Ada should be notified that only team admin can create a board", env -> {
                    pending();
                })
                .Execute();
    }


}
