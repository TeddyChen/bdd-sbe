package tw.teddysoft.bdd.step_definition_exercise.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class TrashBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Trash Board Use Case", """
                A board is moved to the Trash before it is deleted.
                From the users' perspective, a trashed board will not be displayed in its original project.
                That is, the trashed board is simply hidden from users but is still in its original project.
                A trashed board should be visible in its original project when it is restored.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Trashing_an_untrashed_board() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an untrashed board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("he trashes the board", env -> {
                    pending();
                })
                .Then("the board should be found in the trash project", env -> {
                    pending();
                })
                .And("the board should be still in the default project", env ->{
                    pending();
                })
                .But ("the board should not be presented by users via UI", env -> {})
                .Execute();
    }


    @EzScenario
    public void Trashing_a_trashed_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a trashed board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("Teddy trashes the board", env -> {
                    pending();
                })
                .Then("the board does not change its state", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Trashing_a_non_existing_board_should_fail() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .When("he trashes a non-existing $Scrum board", env -> {
                    pending();
                })
                .Then("the operation should fail", env -> {
                    pending();
                })
                .And("he should be notified that the trashed board does not exist", env -> {
                })
                .Execute();
    }

}
