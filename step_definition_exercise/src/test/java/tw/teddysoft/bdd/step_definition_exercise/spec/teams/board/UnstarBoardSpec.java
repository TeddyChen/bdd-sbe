package tw.teddysoft.bdd.step_definition_exercise.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class UnstarBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Unstar Board Use Case", """
                You can unstart boards that are no longer used frequently so that they are removed from the starred project.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Unstaring_a_starred_board_should_remove_it_from_the_starred_project() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an starred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("he unstars the board", env -> {
                    pending();
                })
                .Then("the board should not exist in the starred project", env -> {
                    pending();
                })
                .And("the board is still in the default project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Unstaring_an_unstarred_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an unstarred board named $Scrum in the default project", env ->{
                    pending();
                })
                .When("he unstars the board", env -> {
                    pending();
                })
                .Then("the board should not change its state", env -> {
                    pending();
                })
                .Execute();
    }
}
