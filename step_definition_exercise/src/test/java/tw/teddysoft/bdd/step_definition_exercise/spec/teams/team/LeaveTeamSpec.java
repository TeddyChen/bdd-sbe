package tw.teddysoft.bdd.step_definition_exercise.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class LeaveTeamSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Leave Team Use Case", """
                    A team member can leave a team at anytime.              
                    However, there must be at least one team admin in the team.
                    That is, the last team admin cannot leave the team.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenarioOutline
    public void Leaving_team() {
        String examples = """
                | team_owner_id  | team_role_1 |  user_id_2  | team_role_2 | user_id_3  | team_role_3 | leave_user_id |  result    | note                                    |
                | Teddy          | Admin       |    Ada      | Admin       | Eiffel      | Staff      | Teddy         |  allowed   | there is still one admin in the team    |
                | Teddy          | Admin       |    Ada      | Staff       | Eiffel      | Staff      | Teddy         |  forbidden | the last admin cannot leave             |
                | Teddy          | Admin       |    Ada      | Admin       | Eiffel      | Staff      | Eiffel        |  allowed   | the last staff can leave                |
                """;

        feature.newScenarioOutline()
                .WithExamples(examples)
                .Given("Teddy's team has three member: <team_owner_id>, <user_id_2>, and <user_id_3>", env -> {
                    pending();
                })
                .And("they are with roles <team_role_1>, <team_role_2>, <team_role_3>, respectively.", env ->{
                    pending();
                })
                .When("<leave_user_id> leaves the team", env -> {
                    pending();
                })
                .Then("his leave should be <result> because <note>", env -> {
                    pending();
                })
                .Execute();
    }
}
