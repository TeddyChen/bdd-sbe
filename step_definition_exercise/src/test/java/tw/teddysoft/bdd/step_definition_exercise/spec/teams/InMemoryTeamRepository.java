package tw.teddysoft.bdd.step_definition_exercise.spec.teams;

import tw.teddysoft.bdd.step_definition_exercise.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InMemoryTeamRepository implements TeamRepository {

    List<Team> store = new ArrayList<>();

    @Override
    public Optional<Team> findById(String teamId) {
        return store.stream().filter( x-> x.getId().equals(teamId)).findAny();
    }

    @Override
    public void save(Team data) {
        store.removeIf( x -> x.getId().equals(data.getId()));
        store.add(data);
    }

    @Override
    public void delete(Team data) {
        store.removeIf( x-> x.getId().equals(data.getId()));
    }
}
