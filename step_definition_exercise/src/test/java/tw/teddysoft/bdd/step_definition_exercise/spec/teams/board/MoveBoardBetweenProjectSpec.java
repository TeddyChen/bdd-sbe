package tw.teddysoft.bdd.step_definition_exercise.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class MoveBoardBetweenProjectSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Move Board Between Project Use Case" ,
                """
                          Users can move a board between two [physical] projects.
                          [Physical projects] are the default project and uer-defined projects.
                          Moving a board to its original project is allowed and should not change its state.
                          Note that the Starred and the Trash projects are not physical projects.
                          When a board is starred or trashed, it will still exist in its physical project.
                          """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Moving_a_scrum_board_from_the_default_project_to_the_agile_project() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an $Agile project in his team", env ->{
                    pending();
                })
                .And("a $Scrum board in the default project", env ->{
                    pending();
                })
                .When("he moves the $Scrum board to the $Agile project", env -> {
                    pending();
                })
                .Then("the board should be found in the $Agile project", env -> {
                    pending();
                })
                .And("the board should not exist in the default project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_scrum_board_from_the_agile_project_to_the_default_project() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an $Agile project in his team", env ->{
                    pending();
                })
                .And("a $Scrum board in the $Agile project", env ->{
                    pending();
                })
                .When("he moves the $Scrum board to the default project", env -> {
                    pending();
                })
                .Then("the board should be found in the default project", env -> {
                    pending();
                })
                .And("the board should not exist in the $Agile project", env ->{
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_scrum_board_from_the_default_project_to_a_non_existing_agile_project_should_fail() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a $Scrum board in the default project", env ->{
                    pending();
                })
                .When("he moves the $Scrum board to a non-existing agile project", env -> {
                    pending();
                })
                .Then("the movement should fail", env -> {
                    pending();
                })
                .And("the board should not change its state", env -> {
                    pending();
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_non_existing_scrum_board_to_the_default_project_should_fail() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a $Kanban board in the default project", env ->{
                    pending();
                })
                .When("he moves a non_existing $Scrum board to the default project", env -> {
                    pending();
                })
                .Then("the movement should fail", env -> {
                    pending();
                })
                .And("he should be notified that the board does not exist", env -> {
                    pending();
                })
                .Execute();
    }

}
