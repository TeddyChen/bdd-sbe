package tw.teddysoft.bdd.step_definition_exercise.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_exercise.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_exercise.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_exercise.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class RestoreBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Restore Board Use Case", """
                A board in the Trash can be restored to its original project.
                Restoring a board simply removes the board from the Trash.
                From the users' perspective, a restored board will be displayed in its project.
                In other words, boards that are not in the Trash will be visible to users.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Restoring_a_trashed_board() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("a trashed board named $Scrum in the $Agile project", env ->{
                    pending();
                })
                .When("he restores the board", env -> {
                    pending();
                })
                .Then("the board should not be found in the trash project", env -> {
                    pending();
                })
                .And("the board is presented in the $Agile project", env ->{
                    pending();
                })
                .Execute();
    }


    @EzScenario
    public void Restoring_an_untrashed_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    pending();
                })
                .And("an untrashed board named $Scrum in the $Agile project", env ->{
                    pending();
                })
                .When("Teddy restores the board", env -> {
                    pending();
                })
                .Then("the board does not change its state", env -> {
                    pending();
                })
                .Execute();
    }

}
