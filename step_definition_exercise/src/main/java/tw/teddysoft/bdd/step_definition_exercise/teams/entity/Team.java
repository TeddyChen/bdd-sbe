package tw.teddysoft.bdd.step_definition_exercise.teams.entity;

import tw.teddysoft.ezddd.core.entity.AggregateRoot;
import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.util.*;

public class Team extends AggregateRoot<String, DomainEvent> {
    private String teamId;

    @Override
    public String getId() {
        return teamId;
    }
}
