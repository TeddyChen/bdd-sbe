package tw.teddysoft.bdd.step_definition_answer.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberUseCase;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EzFeature
@EzFeatureReport
public class InviteTeamMemberSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Invite Team Member Use Case", """
                    A team can contain two types of members: Team Admin and Team Staff.                
                    The team creator is a team admin by default.
                    He can invite members as team admins or team staffs.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Inviting_a_user_as_a_team_staff() {
        feature.newScenario()
                .Given("$Teddy is a team admin", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he invites ${userId=Ada} to his team as a team staff", env -> {
                    InviteTeamMemberUseCase inviteTeamMemberUseCase = new InviteTeamMemberService(teamRepository);
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    InviteTeamMemberInput input = new InviteTeamMemberInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setUserId(env.getArg("userId"));
                    input.setTeamRole(TeamRole.Staff);
                    var output = inviteTeamMemberUseCase.execute(input);
                })
                .Then("$Ada should become a team staff of Teddy's team", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    var optTeamMember = team.getTeamMember(env.getArg(0));

                    assertTrue(optTeamMember.isPresent());
                    assertEquals(TeamRole.Staff, optTeamMember.get().teamRole());
                    assertEquals(env.getArg(0), optTeamMember.get().userId());
                })
                .Execute();
    }

    @EzScenario
    public void Inviting_a_user_as_a_team_admin() {
        feature.newScenario()
                .Given("$Teddy is a team admin", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he invites ${userId=Eiffel} to his team as a team admin", env -> {
                    InviteTeamMemberUseCase inviteTeamMemberUseCase = new InviteTeamMemberService(teamRepository);
                    InviteTeamMemberInput input = new InviteTeamMemberInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setUserId(env.getArg("userId"));
                    input.setTeamRole(TeamRole.Admin);
                    inviteTeamMemberUseCase.execute(input);
                })
                .Then("$Eiffel should become a team admin of Teddy's team", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    var optTeamMember = team.getTeamMember(env.getArg(0));

                    assertTrue(optTeamMember.isPresent());
                    assertEquals(TeamRole.Admin, optTeamMember.get().teamRole());
                    assertEquals(env.getArg(0), optTeamMember.get().userId());
                })
                .Execute();
    }

    @EzScenarioOutline
    public void Inviting_team_members_of_different_team_roles() {
        String examples = """
                | invited_user_id | team_role |
                | Ada             | Admin     |
                | Eiffel          | Staff     |
                """;

        feature.newScenarioOutline()
                .WithExamples(examples)
                .Given("$Teddy is a team admin", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he invites <invited_user_id> to his team as a team <team_role>", env -> {
                    InviteTeamMemberUseCase inviteTeamMemberUseCase =
                            new InviteTeamMemberService(teamRepository);
                    InviteTeamMemberInput input = new InviteTeamMemberInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setUserId(env.getInput().get("invited_user_id"));
                    input.setTeamRole(TeamRole.valueOf(env.getInput().get("team_role")));
                    inviteTeamMemberUseCase.execute(input);
                })
                .Then("<invited_user_id> should become a team <team_role> of Teddy's team", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    var optTeamMember = team.getTeamMember(env.getInput().get("invited_user_id"));

                    assertTrue(optTeamMember.isPresent());
                    assertEquals(TeamRole.valueOf(env.getInput().get("team_role")),
                            optTeamMember.get().teamRole());
                    assertEquals(env.getInput().get("invited_user_id"),
                            optTeamMember.get().userId());
                })
                .Execute();
    }
}
