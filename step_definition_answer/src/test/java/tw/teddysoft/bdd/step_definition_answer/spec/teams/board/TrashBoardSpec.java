package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class TrashBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Trash Board Use Case", """
                A board is moved to the Trash before it is deleted.
                From the users' perspective, a trashed board will not be displayed in its original project.
                That is, the trashed board is simply hidden from users but is still in its original project.
                A trashed board should be visible in its original project when it is restored.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Trashing_an_untrashed_board() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an untrashed board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("project", Team.DEFAULT_PROJECT_NAME);
                    env.put("boardId", boardId);
                })
                .When("Teddy trashes the board", env -> {
                    TrashBoardUseCase trashBoardUseCase = new TrashBoardService(teamRepository);
                    TrashBoardInput input = new TrashBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(env.gets("boardId"));
                    trashBoardUseCase.execute(input);
                })
                .Then("the board should be found in the trash project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isPresent());
                })
                .And("the board is still in the default project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }


    @EzScenario
    public void Trashing_a_trashed_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("a trashed board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("project", Team.DEFAULT_PROJECT_NAME);
                    env.put("boardId", boardId);

                    executeTrashBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .When("Teddy trashes the trashed board again", env -> {
                    executeTrashBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .Then("the board should not change its state", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isPresent());
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }

    @EzScenario
    public void Trashing_a_non_existing_board_should_fail() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he trashes a non-existing $Scrum board", env -> {
                    var thrown = assertThrows(
                            IllegalArgumentException.class,
                            () -> executeTrashBoardUseCase(env.gets("teamId"), env.getArg(0), teamRepository),
                            "Trashing a non-existing board should fail");

                    env.put("error", thrown.getMessage());
                })
                .Then("the operation should fail", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("he should be notified that the trashed board does not exist", env -> {
                    assertEquals("Trashed board does not exist", env.gets("error"));
                })
                .Execute();
    }


}
