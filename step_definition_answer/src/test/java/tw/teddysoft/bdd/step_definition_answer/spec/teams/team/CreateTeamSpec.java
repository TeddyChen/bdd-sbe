package tw.teddysoft.bdd.step_definition_answer.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamMember;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamUseCase;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@EzFeature
@EzFeatureReport
public class CreateTeamSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Team Use Case", """
                    A team is created asynchronously after a user registers successfully for ezKanban.                
                    The user becomes the admin of the team (i.e., having the team admin role).
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void If_a_user_registration_is_completed_a_team_belonging_to_the_user_should_be_created() {
        feature.newScenario()
                .Given("a new user named ${userName=Teddy} with user id ${userId=t001}", env -> {
                    env.put("userId", env.getArg("userId"));
                    env.put("userName", env.getArg("userName"));
                })
                .And("he has successfully registered for Miro", env -> {
                    assertTrue(true);
                })
                .When("the registration process is completed, which triggers the automatic creation of a team for Teddy", env -> {
                    CreateTeamUseCase createTeamUseCase = new CreateTeamService(teamRepository);
                    CreateTeamInput input = new CreateTeamInput();
                    input.setTeamId(UUID.randomUUID().toString());
                    input.setTeamName(env.gets("userName"));
                    input.setUserId(env.gets("userId"));
                    CqrsOutput output = createTeamUseCase.execute(input);

                    env.put("teamId", output.getId());
                })
                .Then("a team should be created for Teddy", env -> {
                    var team = teamRepository.findById(env.gets("teamId"));
                    assertTrue(team.isPresent());
                })
                .And("Teddy should be designated as the team admin", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    Optional<TeamMember> teamMember = team.getTeamMember(env.gets("userId"));
                    assertTrue(teamMember.isPresent());
                })
                .Execute();
    }

    @EzScenario
    public void If_a_user_registration_is_completed_a_team_belonging_to_the_user_should_be_created_zh_tw() {
        feature.newScenario()
                .Given("一個使用者 ${userName=Teddy}，他的使用者號碼是 ${userId=t001}", env -> {
                    env.put("userId", env.getArg("userId"));
                    env.put("userName", env.getArg("userName"));
                })
                .And("他成功註冊Miro", env -> {
                    assertTrue(true);
                })
                .When("他的註冊流程完畢,驅動系統自動幫他建立一個Team", env -> {
                    CreateTeamUseCase createTeamUseCase = new CreateTeamService(teamRepository);
                    CreateTeamInput input = new CreateTeamInput();
                    input.setTeamId(UUID.randomUUID().toString());
                    input.setTeamName(env.gets("userName"));
                    input.setUserId(env.gets("userId"));
                    CqrsOutput output = createTeamUseCase.execute(input);

                    env.put("teamId", output.getId());
                })
                .Then("一個屬於Teddy的團隊應該被建立", env -> {
                    var team = teamRepository.findById(env.gets("teamId"));
                    assertTrue(team.isPresent());
                })
                .And("Teddy應該被指派為team admin", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    Optional<TeamMember> teamMember = team.getTeamMember(env.gets("userId"));
                    assertTrue(teamMember.isPresent());
                })
                .Execute();
    }
}
