package tw.teddysoft.bdd.step_definition_answer.spec.teams;


import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash.TrashBoardUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create.CreateTeamUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite.InviteTeamMemberUseCase;

public class TeamTestContext {

    public static String executeCreateTeamUseCase(String teamId, String teamName, String userId, TeamRepository teamRepository){
        CreateTeamUseCase createTeamUseCase = new CreateTeamService(teamRepository);
        CreateTeamInput input = new CreateTeamInput();
        input.setTeamId(teamId);
        input.setTeamName(teamName);
        input.setUserId(userId);
        return createTeamUseCase.execute(input).getId();
    }

    public static String executeInviteTeamMember(String teamId, String userId, TeamRole teamRole, TeamRepository teamRepository){
        InviteTeamMemberUseCase inviteTeamMemberUseCase =
                new InviteTeamMemberService(teamRepository);
        InviteTeamMemberInput input = new InviteTeamMemberInput();
        input.setTeamId(teamId);
        input.setUserId(userId);
        input.setTeamRole(teamRole);
        var output = inviteTeamMemberUseCase.execute(input);

        return output.getId();
    }

    public static String executeCreateProjectUseCase(String teamId, String projectName, TeamRepository teamRepository){
        CreateProjectUseCase createProjectUseCase = new CreateProjectService(teamRepository);
        CreateProjectInput input = new CreateProjectInput();
        input.setTeamId(teamId);
        input.setProjectName(projectName);
        return createProjectUseCase.execute(input).getId();
    }

    public static String executeCreateBoardInDefaultProjectUseCase(String teamId, String boardId, String boardName, String userId, TeamRepository teamRepository){
        return executeCreateBoardInUserDefinedProjectUseCase(teamId, boardId, boardName, null, userId, teamRepository);
    }

    public static String executeCreateBoardInUserDefinedProjectUseCase(String teamId, String boardId, String boardName, String projectName, String userId, TeamRepository teamRepository){
        CreateBoardUseCase createBoardUseCase = new CreateBoardService(teamRepository);
        CreateBoardInput input = new CreateBoardInput();
        input.setTeamId(teamId);
        input.setBoardId(boardId);
        input.setBoardName(boardName);
        input.setProjectName(projectName);
        input.setUserId(userId);
        return createBoardUseCase.execute(input).getId();
    }
    public static String executeMoveBoardBetweenProjectsUseCase(String teamId, String toProjectName, String boardId, TeamRepository teamRepository){
        MoveBoardBetweenProjectUseCase moveBoardBetweenProjectUseCase = new MoveBoardBetweenProjectService(teamRepository);
        MoveBoardBetweenProjectInput input = new MoveBoardBetweenProjectInput();
        input.setTeamId(teamId);
        input.setToProjectName(toProjectName);
        input.setBoardId(boardId);
        return moveBoardBetweenProjectUseCase.execute(input).getId();
    }

    public static String executeTrashBoardUseCase(String teamId, String boardId, TeamRepository teamRepository){
        TrashBoardUseCase trashBoardUseCase = new TrashBoardService(teamRepository);
        TrashBoardInput input = new TrashBoardInput();
        input.setTeamId(teamId);
        input.setBoardId(boardId);
        return trashBoardUseCase.execute(input).getId();
    }

    public static String executeStarBoardUseCase(String teamId, String boardId, TeamRepository teamRepository){
        StarBoardUseCase starBoardUseCase = new StarBoardService(teamRepository);
        StarBoardInput input = new StarBoardInput();
        input.setTeamId(teamId);
        input.setBoardId(boardId);
        return starBoardUseCase.execute(input).getId();
    }

    public static String executeUnstarBoardUseCase(String teamId, String boardId, TeamRepository teamRepository){
        UnstarBoardUseCase unstarBoardUseCase = new UnstarBoardService(teamRepository);
        UnstarBoardInput input = new UnstarBoardInput();
        input.setTeamId(teamId);
        input.setBoardId(boardId);
        return unstarBoardUseCase.execute(input).getId();
    }

}
