package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.BoardRole;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Project;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create.CreateBoardUseCase;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;

@EzFeature
@EzFeatureReport
public class CreateBoardSpec {
    private TeamRepository teamRepository;
    static Feature feature;
    static final String Rule1_BoardCanOnlyBeCreatedInPhysicalProject = "Boards can only be created in physical projects";
    static final String Rul2_eBoardNameCanBeDuplicated = "Board names can be duplicated";
    static final String Rule3_OnlyTeamAdminCanCreateBoard = "Only team admins can creates boards and the creator becomes the board admin";
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Board Use Case", """
                In order to collaborate with my friends 
                As a Miro user
                I want to create a board
                """);
        feature.newRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject);
        feature.newRule(Rul2_eBoardNameCanBeDuplicated);
        feature.newRule(Rule3_OnlyTeamAdminCanCreateBoard);
    }
    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();

        feature.withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject).newBackground("")
                .Given("a registered user ${userName=Teddy}", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg("userName"),
                            env.gets("userId"),
                            teamRepository);

                    env.put("userName", env.getArg("userName"));
                    env.put("teamId", teamId);
                })
                .Execute();

        feature.withRule(Rule3_OnlyTeamAdminCanCreateBoard).newBackground("")
                .Given("a registered user ${userName=Pascal}", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg("userName"),
                            env.gets("userId"),
                            teamRepository);

                    env.put("userName", env.getArg("userName"));
                    env.put("teamId", teamId);
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_without_specifying_its_project_should_create_it_in_the_default_project() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he creates a $Scrum board without specifying a target project", env -> {
                    assertEquals("Teddy", env.gets("userName"));
                    CreateBoardUseCase createBoardUseCase = new CreateBoardService(teamRepository);
                    CreateBoardInput input = new CreateBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(UUID.randomUUID().toString());
                    input.setBoardName(env.getArg(0));
                    input.setUserId(env.gets("userId"));
                    var output = createBoardUseCase.execute(input);

                    env.put("boardId", output.getId());
                    env.put("boardName", env.getArg(0));
                })
                .Then("the board should be created in the default project of the team", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                    assertEquals(env.gets("boardName"), team.getDefaultProject().getBoard(env.gets("boardId")).get().getName());
                })
                .Execute();
    }


    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_a_user_defined_project() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .And("a ${projectName=BDD} project belongs to his team", env ->{
                    executeCreateProjectUseCase(env.gets("teamId"), env.getArg("projectName"), teamRepository);

                    env.put("projectName", env.getArg("projectName"));
                })
                .When("he creates a $Kanban board in the BDD project", env -> {
                    CreateBoardUseCase createBoardUseCase = new CreateBoardService(teamRepository);
                    CreateBoardInput input = new CreateBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId("board_id");
                    input.setBoardName(env.getArg(0));
                    input.setUserId(env.gets("userId"));
                    input.setProjectName(env.gets("projectName"));
                    var output = createBoardUseCase.execute(input);

                    env.put("boardId", output.getId());
                    env.put("boardName", env.getArg(0));
                })
                .Then("the board should exist in the BDD project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    Project project = team.getUserDefinedProject(env.gets("projectName")).get();
                    assertTrue(project.getBoard(env.gets("boardId")).isPresent());
                    assertEquals(env.gets("boardName"), project.getBoard(env.gets("boardId")).get().getName());
                })
                .And("the board should not in the default project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("projectName")).isEmpty());
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_a_non_existing_user_defined_project_should_fail() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he creates a $Scrum board in a non-existing $BDD project", env -> {
                    assertEquals("Teddy", env.gets("userName"));
                    env.put("project", env.getArg(1));
                    try{
                        CreateBoardUseCase createBoardUseCase = new CreateBoardService(teamRepository);
                        CreateBoardInput input = new CreateBoardInput();
                        input.setTeamId(env.gets("teamId"));
                        input.setBoardId(env.getArg(0));
                        input.setBoardName(env.getArg(0));
                        input.setUserId(env.gets("userId"));
                        input.setProjectName(env.getArg(1));
                        createBoardUseCase.execute(input);
                    }
                    catch (IllegalArgumentException e){
                        env.put("error", e.getMessage());
                    }
                })
                .Then("the creation should be failed", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("he should be notified that the BDD project does not exist", env -> {
                    assertTrue(env.gets("error").contains("Project does not exist: "  + env.gets("project")));
                })
                .Execute();
    }

    @EzScenario(rule = Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
    public void Creating_a_board_in_the_star_should_not_be_allowed() {
        feature.newScenario().withRule(Rule1_BoardCanOnlyBeCreatedInPhysicalProject)
                .When("he wants to create a Scrum board in the $Starred project", env -> {
                    try{
                        executeCreateBoardInUserDefinedProjectUseCase(
                                env.gets("teamId"),
                                env.gets("boardId1"),
                                env.gets("boardName"),
                                env.getArg(0),
                                env.gets("userId"),
                                teamRepository);
                    }catch (Throwable e){
                        env.put("error", e.getMessage());
                    }

                })
                .Then("there is no way to do so because boards can be created only be created in physical projects", env -> {
                    assertEquals("Project does not exist: Starred", env.gets("error"));
                })
                .Execute();
    }


    @EzScenario(rule = Rul2_eBoardNameCanBeDuplicated)
    public void Creating_a_board_with_duplicated_board_name_should_be_allowed() {
        feature.newScenario().withRule(Rul2_eBoardNameCanBeDuplicated)
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    env.put("userName", env.getArg(0));
                    var teamId = executeCreateTeamUseCase(
                            env.gets("userId"),
                            env.gets("userName"),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("a $Scrum board in the default project", env -> {
                    env.put("boardId1", UUID.randomUUID().toString());
                    env.put("boardName", env.getArg(0));
                    executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            env.gets("boardId1"),
                            env.gets("boardName"),
                            env.gets("userId"),
                            teamRepository);
                })
                .When("he creates a board named $Scrum in the default project", env -> {
                    env.put("boardId2", UUID.randomUUID().toString());
                    executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            env.gets("boardId2"),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                })
                .Then("the second $Scrum board should be created in the default project of the team", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId2")).isPresent());
                })
                .And("there should be two boards named $Scrum in the default project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertEquals(env.gets("boardName"), team.getDefaultProject().getBoard(env.gets("boardId1")).get().getName());
                    assertEquals(env.gets("boardName"), team.getDefaultProject().getBoard(env.gets("boardId2")).get().getName());
                })
                .Execute();
    }



    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void Only_team_admins_can_create_a_board_and_becomes_the_board_admin() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .When("he creates a $Scrum board in the default project", env -> {
                    assertEquals("Pascal", env.gets("userName"));
                    env.put("boardId", UUID.randomUUID().toString());
                    try{
                        executeCreateBoardInDefaultProjectUseCase(
                                env.gets("teamId"),
                                env.gets("boardId"),
                                env.getArg(0),
                                env.gets("userId"),
                                teamRepository);
                    }
                    catch (Throwable e){
                        env.put("error", e.getMessage());
                    }
                })
                .Then("the creation should succeed", env -> {
                    assertEquals("", env.gets("error"));
                })
                .And("the board creator Pascal who is the team admin should become the board admin", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    var board = team.getDefaultProject().getBoard(env.gets("boardId")).get();
                    assertEquals(BoardRole.Admin, board.getBoardMember(env.gets("userId")).get().boardRole());
                })
                .Execute();
    }

    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void A_team_staff_should_not_be_able_to_create_a_board() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .And("$Ada is a team staff of his team", env -> {
                    env.put("userId", env.getArg(0));
                    executeInviteTeamMember(env.gets("teamId"), env.gets("userId"), TeamRole.Staff, teamRepository);
                    assertEquals("Pascal", env.gets("userName"));
                })
                .When("$Ada creates a board named $Scrum in the default project of his team", env -> {
                    env.put("boardName", env.getArg(0));
                    try {
                        executeCreateBoardInDefaultProjectUseCase(
                                env.gets("teamId"),
                                UUID.randomUUID().toString(),
                                env.gets("boardName"),
                                env.gets("userId"), teamRepository);
                    }
                    catch (Exception e){
                        env.put("error", e.getMessage());
                    }
                })
                .Then("the creation should be failed", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("$Ada should be notified that only team admin can create a board", env -> {
                    assertEquals("The user who is a team staff cannot create a board. Only a team admin can.", env.gets("error"));
                })
                .Execute();
    }

    @EzScenario(rule = Rule3_OnlyTeamAdminCanCreateBoard)
    public void A_user_who_does_not_belong_to_a_team_should_not_be_able_to_create_a_board() {
        feature.newScenario().withRule(Rule3_OnlyTeamAdminCanCreateBoard)
                .When("$Ada who does not belong to his team creates a board", env -> {
                    env.put("boardName", env.getArg(0));
                    try {
                        executeCreateBoardInDefaultProjectUseCase(
                                env.gets("teamId"),
                                UUID.randomUUID().toString(),
                                env.gets("boardName"),
                                env.getArg(0), teamRepository);
                    }
                    catch (Exception e){
                        env.put("error", e.getMessage());
                    }
                })
                .Then("the creation should be failed", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("$Ada should be notified that users who do not belong to the team cannot create a board", env -> {
                    assertEquals("The user who is not a team member cannot create a board.", env.gets("error"));
                })
                .Execute();
    }

}
