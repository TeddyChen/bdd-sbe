package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.restore.RestoreBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.restore.RestoreBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.restore.RestoreBoardUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class RestoreBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Restore Board Use Case", """
                A board in the Trash can be restored to its original project.
                Restoring a board simply removes the board from the Trash.
                From the users' perspective, a restored board will be displayed in its project.
                In other words, boards that are not in the Trash will be visible to users.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Restoring_a_trashed_board() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("a trashed board named $Scrum in the $Agile project", env ->{
                    env.put("boardName", env.getArg(0));
                    env.put("projectName", env.getArg(1));
                    executeCreateProjectUseCase(env.gets("teamId"), env.gets("projectName"), teamRepository);
                    String boardId = executeCreateBoardInUserDefinedProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.gets("boardName"),
                            env.gets("projectName"),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardId", boardId);

                    executeTrashBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .When("he restores the board", env -> {
                    RestoreBoardUseCase restoreBoardUseCase = new RestoreBoardService(teamRepository);
                    RestoreBoardInput input = new RestoreBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(env.gets("boardId"));
                    restoreBoardUseCase.execute(input);
                })
                .Then("the board should not be found in the trash project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isEmpty());
                })
                .And("the board is presented in the $Agile project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getUserDefinedProject(env.gets("projectName")).get().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }


    @EzScenario
    public void Restoring_an_untrashed_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an untrashed board named $Scrum in the $Agile project", env ->{
                    env.put("boardName", env.getArg(0));
                    env.put("projectName", env.getArg(1));
                    executeCreateProjectUseCase(env.gets("teamId"), env.gets("projectName"), teamRepository);
                    String boardId = executeCreateBoardInUserDefinedProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.gets("boardName"),
                            env.gets("projectName"),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardId", boardId);
                })
                .When("Teddy restores the board", env -> {
                    RestoreBoardUseCase restoreBoardUseCase = new RestoreBoardService(teamRepository);
                    RestoreBoardInput input = new RestoreBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(env.gets("boardId"));
                    restoreBoardUseCase.execute(input);
                })
                .Then("the board does not change its state", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isEmpty());
                    assertTrue(team.getUserDefinedProject(env.gets("projectName")).get().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }

}
