package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star.StarBoardUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class StarBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Star Board Use Case", """
                You can start boards that are frequently used so that they can be quickly found in the starred project.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Staring_an_unstarred_board_should_add_it_to_the_starred_project() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an unstarred board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("projectName", Team.DEFAULT_PROJECT_NAME);
                    env.put("boardId", boardId);
                })
                .When("he stars the board", env -> {
                    StarBoardUseCase starBoardUseCase = new StarBoardService(teamRepository);
                    StarBoardInput input = new StarBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(env.gets("boardId"));
                    starBoardUseCase.execute(input);
                })
                .Then("the board should be found in the starred project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getStarred().getBoard(env.gets("boardId")).isPresent());
                })
                .And("the board should be still in the default project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }


    @EzScenario
    public void Staring_a_starred_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an starred board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("projectName", Team.DEFAULT_PROJECT_NAME);
                    env.put("boardId", boardId);

                    executeStarBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .When("he stars the board again", env -> {
                    executeStarBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .Then("the board should not change its state", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getStarred().getBoard(env.gets("boardId")).isPresent());
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }

    @EzScenario
    public void Staring_a_non_existing_board_should_fail() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he stars a non-existing $Scrum board", env -> {
                    var thrown = assertThrows(
                            IllegalArgumentException.class,
                            () -> executeStarBoardUseCase(env.gets("teamId"), env.getArg(0), teamRepository),
                            "Staring a non-existing board should fail");

                    env.put("error", thrown.getMessage());

                })
                .Then("the operation should fail", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("he should be notified that the starred board does not exist", env -> {
                    assertEquals("Starred board does not exist", env.gets("error"));
                })
                .Execute();
    }
}
