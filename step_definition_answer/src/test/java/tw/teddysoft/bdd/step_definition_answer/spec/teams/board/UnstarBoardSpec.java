package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar.UnstarBoardUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class UnstarBoardSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Unstar Board Use Case", """
                You can unstart boards that are no longer used frequently so that they are removed from the starred project.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Unstaring_a_starred_board_should_remove_it_from_the_starred_project() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an starred board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("boardId", boardId);

                    executeStarBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .When("he unstars the board", env -> {
                    UnstarBoardUseCase unstarBoardUseCase = new UnstarBoardService(teamRepository);
                    UnstarBoardInput input = new UnstarBoardInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setBoardId(env.gets("boardId"));
                    unstarBoardUseCase.execute(input);
                })
                .Then("the board should not exist in the starred project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isEmpty());
                })
                .And("the board is still in the default project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }

    @EzScenario
    public void Unstaring_an_unstarred_board_should_not_change_its_state() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an unstarred board named $Scrum in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("boardId", boardId);
                })
                .When("he unstars the board", env -> {
                    executeUnstarBoardUseCase(env.gets("teamId"), env.gets("boardId"), teamRepository);
                })
                .Then("the board should not change its state", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getStarred().getBoard(env.gets("boardId")).isEmpty());
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .Execute();
    }
}
