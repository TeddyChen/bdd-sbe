package tw.teddysoft.bdd.step_definition_answer.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.ProjectDto;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard.GetDashboardInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard.GetDashboardOutput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard.GetDashboardService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard.GetDashboardUseCase;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;

@EzFeature
@EzFeatureReport
public class GetDashboardSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Get Dashboard Use Case", """
                    This is a query to get the view model for clients to render UI for the dashboard.               
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void If_Teddy_logged_in_successfully_he_should_see_a_personal_dashboard() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    env.put("userName", env.getArg(0));
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.gets("userName"),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("two user defined projects: $DDD and $CA", env -> {
                    executeCreateProjectUseCase(env.gets("teamId"), env.getArg(0), teamRepository);
                    executeCreateProjectUseCase(env.gets("teamId"), env.getArg(1), teamRepository);
                })
                .And("a $Scrum board in the default project", env -> {
                    executeCreateBoardInDefaultProjectUseCase(env.gets("teamId"), env.getArg(0),env.getArg(0), env.gets("userId"), teamRepository);
                })
                .And("a $Kanban board and a $Pattern board in the $DDD project", env -> {
                    executeCreateBoardInUserDefinedProjectUseCase(env.gets("teamId"), env.getArg(0),env.getArg(0), env.getArg(2), env.gets("userId"), teamRepository);
                    executeCreateBoardInUserDefinedProjectUseCase(env.gets("teamId"), env.getArg(1),env.getArg(1), env.getArg(2), env.gets("userId"), teamRepository);
                })
                .And("the $Scrum board has been starred", env -> {
                    executeStarBoardUseCase(env.gets("teamId"), env.getArg(0), teamRepository);
                })
                .And("the $Pattern board has been trashed", env -> {
                    executeTrashBoardUseCase(env.gets("teamId"), env.getArg(0), teamRepository);
                })
                .When("he logs in Miro", env -> {
                    GetDashboardUseCase getDashboardUseCase = new GetDashboardService(teamRepository);
                    GetDashboardInput input = new GetDashboardInput();
                    input.setTeamId(env.gets("teamId"));
                    GetDashboardOutput output = getDashboardUseCase.execute(input);

                    env.put("output", output);
                })
                .Then("he should see a dashboard containing two user defined projects", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    assertEquals(2, output.getTeamDto().getUserDefinedProjects().size());
                })
                .And("the $Kanban board should be in the $DDD project", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    ProjectDto dddProject = output.getTeamDto().getUserDefinedProjects().stream().filter(x -> x.getName().equals(env.getArg(1))).findAny().get();
                    assertTrue(dddProject.getBoards().stream().anyMatch(x -> x.getName().equals(env.getArg(0))));

                    env.put("dddProject", dddProject);
                })
                .And("the $Pattern board should not exist in the $DDD project", env -> {
                    assertFalse(env.get("dddProject", ProjectDto.class).getBoards().stream().anyMatch(x -> x.getName().equals(env.getArg(0))));
                })
                .And("the $Pattern board should be in the trash project", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    assertTrue(output.getTeamDto().getTrash().getBoards().stream().anyMatch(x -> x.getName().equals(env.getArg(0))));

                })
                .And("the $Scrum board should be in both the default and the starred projects", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    assertTrue(output.getTeamDto().getDefaultProject().getBoards().stream().anyMatch(x -> x.getName().equals(env.getArg(0))));
                    assertTrue(output.getTeamDto().getStarred().getBoards().stream().anyMatch(x -> x.getName().equals(env.getArg(0))));
                })
                .And("the team should contain a team admin", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    assertEquals(1, output.getTeamDto().getTeamMembers().size());
                    assertEquals(env.gets("userId"), output.getTeamDto().getTeamMembers().get(0).getUserId());
                    assertEquals(TeamRole.Admin.name(), output.getTeamDto().getTeamMembers().get(0).getRole());
                })
                .Execute();
    }

    @EzScenario
    public void If_Teddy_logged_in_successfully_he_should_see_a_personal_dashboard_table_arguments_version() {
        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    env.put("userName", env.getArg(0));
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.gets("userName"),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("the following user defined projects:\n" + """
                        | user_defined_project |
                        | DDD                  |
                        | CA                   |""",    env ->{

                    env.table().rows().forEach( row -> {
                        executeCreateProjectUseCase(env.gets("teamId"), row.get("user_defined_project"), teamRepository);
                    });
                    env.put("p1", env.row(0).get("user_defined_project"));
                    env.put("p2", env.row(1).get("user_defined_project"));
                })
                .And("the following boards:\n" + """
                        | board_name    |  in_project   |   starred |   trashed |
                        | Scrum         |   DEFAULT     |   true    |   false   |
                        | Kanban        |   DDD         |   false   |   false   |
                        | Pattern       |   DDD         |   false   |   true    |
                        """, env -> {

                    env.table().rows().forEach( row -> {
                        switch (row.get("in_project")){
                            case "DEFAULT" -> executeCreateBoardInDefaultProjectUseCase(env.gets("teamId"), row.get("board_name"), row.get("board_name"), env.gets("userId"), teamRepository);
                            case "DDD" -> executeCreateBoardInUserDefinedProjectUseCase(env.gets("teamId"), row.get("board_name"), row.get("board_name"), row.get("in_project"), env.gets("userId"), teamRepository);
                            default -> throw new RuntimeException("Undefined project");
                        }

                        if (row.get("starred").equalsIgnoreCase("true")){
                            executeStarBoardUseCase(env.gets("teamId"), row.get("board_name"), teamRepository);
                        }

                        if (row.get("trashed").equalsIgnoreCase("true")){
                            executeTrashBoardUseCase(env.gets("teamId"), row.get("board_name"), teamRepository);
                        }
                    });
                })
                .When("he logs in Miro", env -> {
                    GetDashboardUseCase getDashboardUseCase = new GetDashboardService(teamRepository);
                    GetDashboardInput input = new GetDashboardInput();
                    input.setTeamId(env.gets("teamId"));
                    GetDashboardOutput output = getDashboardUseCase.execute(input);

                    env.put("output", output);
                })
                .Then("he should see a dashboard as followings:\n" + """
                        | board_name    |   in_default |   in_ddd_project     |  in_ca_project    |   in_starred_project |  in_trashed_project |
                        | Scrum         |   true       |   false              |  false            |       true           |      false          |
                        | Kanban        |   false      |   true               |  false            |       false          |      false          | 
                        | Pattern       |   false      |   false              |  false            |       false          |      true           |
                        """, env -> {

                    var output = env.get("output", GetDashboardOutput.class);
                    assertEquals(2, output.getTeamDto().getUserDefinedProjects().size());

                    env.table().rows().forEach( row -> {
                        assertEquals(Boolean.valueOf(row.get("in_default")), output.getTeamDto().getDefaultProject().getBoards().stream().anyMatch(x -> x.getName().equals(row.get("board_name"))));

                        ProjectDto dddProject = output.getTeamDto().getUserDefinedProjects().stream().filter(x -> x.getName().equals("DDD")).findAny().get();
                        assertEquals(Boolean.valueOf(row.get("in_ddd_project")), dddProject.getBoards().stream().anyMatch(x -> x.getName().equals(row.get("board_name"))));

                        ProjectDto caProject = output.getTeamDto().getUserDefinedProjects().stream().filter(x -> x.getName().equals("CA")).findAny().get();
                        assertEquals(Boolean.valueOf(row.get("in_ca_project")), caProject.getBoards().stream().anyMatch(x -> x.getName().equals(row.get("board_name"))));

                        assertEquals(Boolean.valueOf(row.get("in_starred_project")), output.getTeamDto().getStarred().getBoards().stream().anyMatch(x -> x.getName().equals(row.get("board_name"))));
                        assertEquals(Boolean.valueOf(row.get("in_trashed_project")), output.getTeamDto().getTrash().getBoards().stream().anyMatch(x -> x.getName().equals(row.get("board_name"))));
                    });
                })
                .And("the team should contain a team admin Teddy", env -> {
                    var output = env.get("output", GetDashboardOutput.class);
                    assertEquals(1, output.getTeamDto().getTeamMembers().size());
                    assertEquals(env.gets("userId"), output.getTeamDto().getTeamMembers().get(0).getUserId());
                    assertEquals(TeamRole.Admin.name(), output.getTeamDto().getTeamMembers().get(0).getRole());
                })
                .Execute();
    }
}
