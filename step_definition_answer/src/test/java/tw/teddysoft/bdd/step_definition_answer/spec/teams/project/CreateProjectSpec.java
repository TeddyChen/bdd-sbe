package tw.teddysoft.bdd.step_definition_answer.spec.teams.project;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create.CreateProjectUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class CreateProjectSpec {
    static Feature feature;
    private TeamRepository teamRepository;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Create Project Use Case", """
                Users create projects to categorize their boards.
                The project name cannot be duplicated.
                Projects can be created directly under the user's team.
                That is, nested project structures are not supported.
                """);
    }
    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenario
    public void Creating_a_project_with_unique_name() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .When("he creates a project named $SBE", env -> {
                    CreateProjectUseCase createProjectUseCase = new CreateProjectService(teamRepository);
                    CreateProjectInput input = new CreateProjectInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setProjectName(env.getArg(0));
                    createProjectUseCase.execute(input);

                    env.put("projectName", env.getArg(0));
                })
                .Then("the team should contain a new project named SBE", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getUserDefinedProject(env.gets("projectName")).isPresent());
                })
                .Execute();
    }

    @EzScenario
    public void Cannot_create_a_project_with_duplicated_project_named() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.getArg(0),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("a $DDD project belongs to his team", env ->{
                    CreateProjectUseCase createProjectUseCase = new CreateProjectService(teamRepository);
                    CreateProjectInput input = new CreateProjectInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setProjectName(env.getArg(0));
                    createProjectUseCase.execute(input);

                    env.put("projectName", env.getArg(0));
                })
                .When("he creates a project named $DDD", env -> {
                    try{
                        CreateProjectUseCase createProjectUseCase = new CreateProjectService(teamRepository);
                        CreateProjectInput input = new CreateProjectInput();
                        input.setTeamId(env.gets("teamId"));
                        input.setProjectName(env.getArg(0));
                        createProjectUseCase.execute(input);
                    }
                    catch (IllegalArgumentException e){
                        env.put("error", e.getMessage());
                    }
                })
                .Then("the second DDD project should not be created due to project name duplication", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertEquals(1, team.getUserDefinedProjects().size());
                    assertEquals("Project name duplicated: DDD", env.gets("error"));
                })
                .Execute();
    }
}
