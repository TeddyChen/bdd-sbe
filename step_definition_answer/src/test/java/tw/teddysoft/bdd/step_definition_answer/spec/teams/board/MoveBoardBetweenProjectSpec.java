package tw.teddysoft.bdd.step_definition_answer.spec.teams.board;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move.MoveBoardBetweenProjectUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;

@EzFeature
@EzFeatureReport
public class MoveBoardBetweenProjectSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Move Board Between Project Use Case" ,
                """
                          Users can move a board between two [physical] projects.
                          [Physical projects] are the default project and uer-defined projects.
                          Moving a board to its original project is allowed and should not change its state.
                          Note that the Starred and the Trash projects are not physical projects.
                          When a board is starred or trashed, it will still exist in its physical project.
                          """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }
    @EzScenario
    public void Moving_a_scrum_board_from_the_default_project_to_the_agile_project() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an empty $Agile project in his team", env ->{
                    executeCreateProjectUseCase(env.gets("teamId"), env.getArg(0), teamRepository);
                    env.put("toProject", env.getArg(0));
                })
                .And("a $Scrum board in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(env.gets("teamId"), UUID.randomUUID().toString(), env.getArg(0), env.gets("userId"), teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("fromProject", Team.DEFAULT_PROJECT_NAME);
                    env.put("boardId", boardId);
                })
                .When("he moves the $Scrum board to the $Agile project", env -> {
                    MoveBoardBetweenProjectUseCase moveBoardBetweenProjectUseCase = new MoveBoardBetweenProjectService(teamRepository);
                    MoveBoardBetweenProjectInput input = new MoveBoardBetweenProjectInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setToProjectName(env.gets("toProject"));
                    input.setBoardId(env.gets("boardId"));
                    moveBoardBetweenProjectUseCase.execute(input);
                })
                .Then("the $Scrum board should be found in the $Agile project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getUserDefinedProject(env.gets("toProject")).get().getBoard(env.gets("boardId")).isPresent());
                })
                .And("the $Scrum board should not exist in the default project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isEmpty());
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_scrum_board_from_the_agile_project_to_the_default_project() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("an $Agile project in his team", env ->{
                    executeCreateProjectUseCase(env.gets("teamId"), env.getArg(0), teamRepository);
                })
                .And("a $Scrum board in the $Agile project", env ->{
                    env.put("boardName", env.getArg(0));
                    env.put("fromProject", env.getArg(1));
                    String boardId = executeCreateBoardInUserDefinedProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.gets("boardName"),
                            env.gets("fromProject"),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardId", boardId);
                })
                .When("he moves the Scrum board to the default project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    executeMoveBoardBetweenProjectsUseCase(env.gets("teamId"), Team.DEFAULT_PROJECT_NAME, env.gets("boardId"), teamRepository);
                })
                .Then("the board should be found in the default project", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                })
                .And("the board should not exist in the $Agile project", env ->{
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getUserDefinedProject(env.gets("fromProject")).get().getBoard(env.gets("boardId")).isEmpty());
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_scrum_board_from_the_default_project_to_a_non_existing_agile_project_should_fail() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                        env.put("userId", UUID.randomUUID().toString());
                        var teamId = executeCreateTeamUseCase(
                                UUID.randomUUID().toString(),
                                env.getArg(0),
                                env.gets("userId"),
                                teamRepository);

                        env.put("teamId", teamId);
                })
                .And("a $Scrum board in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                    env.put("boardName", env.getArg(0));
                    env.put("boardId", boardId);
                })
                .When("he moves the $Scrum board to a non-existing agile project", env -> {
                    var thrown = assertThrows(
                            IllegalArgumentException.class,
                            () -> executeMoveBoardBetweenProjectsUseCase(env.gets("teamId"),"agile", env.gets("boardId"), teamRepository),
                            "Move a board to a non-existing project should fail");

                    env.put("error", thrown.getMessage());
                })
                .Then("the movement should fail", env -> {
                    assertTrue(env.gets("error").contains("To project not found"));
                })
                .And("the board should not change its state", env -> {
                    Team team = teamRepository.findById(env.gets("teamId")).get();
                    assertTrue(team.getDefaultProject().getBoard(env.gets("boardId")).isPresent());
                    assertTrue(team.getTrash().getBoard(env.gets("boardId")).isEmpty());
                    assertTrue(team.getStarred().getBoard(env.gets("boardId")).isEmpty());
                })
                .Execute();
    }

    @EzScenario
    public void Moving_a_non_existing_scrum_board_to_the_default_project_should_fail() {

        feature.newScenario()
                .Given("a registered user $Teddy", env -> {
                    env.put("userId", UUID.randomUUID().toString());
                    var teamId = executeCreateTeamUseCase(
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);

                    env.put("teamId", teamId);
                })
                .And("a $Kanban board in the default project", env ->{
                    String boardId = executeCreateBoardInDefaultProjectUseCase(
                            env.gets("teamId"),
                            UUID.randomUUID().toString(),
                            env.getArg(0),
                            env.gets("userId"),
                            teamRepository);
                })
                .When("he moves a non_existing $Scrum board to the default project", env -> {
                    var thrown = assertThrows(
                            IllegalArgumentException.class,
                            () -> executeMoveBoardBetweenProjectsUseCase(env.gets("teamId"), Team.DEFAULT_PROJECT_NAME, env.getArg(0), teamRepository),
                            "Attempting to move a not-existing board to the default project should fail.");

                    env.put("error", thrown.getMessage());
                })
                .Then("the movement should fail", env -> {
                    assertFalse(env.gets("error").isEmpty());
                })
                .And("he should be notified that the board does not exist", env -> {
                    assertTrue(env.gets("error").contains("The board does not exit"));
                })
                .Execute();
    }
}
