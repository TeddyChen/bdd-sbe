package tw.teddysoft.bdd.step_definition_answer.spec.teams.team;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.InMemoryTeamRepository;
import tw.teddysoft.bdd.step_definition_answer.spec.teams.TeamTestContext;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave.LeaveTeamInput;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave.LeaveTeamService;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave.LeaveTeamUseCase;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.EzFeatureReport;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezspec.keyword.Feature;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@EzFeature
@EzFeatureReport
public class LeaveTeamSpec {
    static Feature feature;
    private TeamRepository teamRepository;
    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Leave Team Use Case", """
                    A team member can leave a team at anytime.              
                    However, there must be at least one team admin in the team.
                    That is, the last team admin cannot leave the team.
                """);
    }

    @BeforeEach
    public void setup(){
        teamRepository = new InMemoryTeamRepository();
    }

    @EzScenarioOutline
    public void Leaving_team() {
        String examples = """
                | team_owner_id  | team_role_1 |  user_id_2  | team_role_2 | user_id_3  | team_role_3 | leave_user_id |  result    | note                                    |
                | Teddy          | Admin       |    Ada      | Admin       | Eiffel      | Staff      | Teddy         |  allowed   | there is still one admin in the team    |
                | Teddy          | Admin       |    Ada      | Staff       | Eiffel      | Staff      | Teddy         |  forbidden | the last admin cannot leave             |
                | Teddy          | Admin       |    Ada      | Admin       | Eiffel      | Staff      | Eiffel        |  allowed   | the last staff can leave                |
                """;

        feature.newScenarioOutline()
                .WithExamples(examples)
                .Given("Teddy's team has three member: <team_owner_id>, <user_id_2>, and <user_id_3>", env -> {
                    var teamId = TeamTestContext.executeCreateTeamUseCase(UUID.randomUUID().toString(),
                            env.getInput().get("team_owner_id"),
                            env.getInput().get("team_owner_id"),
                            teamRepository);
                    env.put("teamId", teamId);
                })
                .And("they are with roles <team_role_1>, <team_role_2>, <team_role_3>, respectively.", env ->{

                    TeamTestContext.executeInviteTeamMember(
                            env.gets("teamId"),
                            env.getInput().get("user_id_2"),
                            TeamRole.valueOf(env.getInput().get("team_role_2")),
                            teamRepository);

                    TeamTestContext.executeInviteTeamMember(
                            env.gets("teamId"),
                            env.getInput().get("user_id_3"),
                            TeamRole.valueOf(env.getInput().get("team_role_3")),
                            teamRepository);
                })
                .When("<leave_user_id> leaves the team", env -> {
                    LeaveTeamUseCase leaveTeamUseCase = new LeaveTeamService(teamRepository);
                    LeaveTeamInput input = new LeaveTeamInput();
                    input.setTeamId(env.gets("teamId"));
                    input.setUserId(env.getInput().get("leave_user_id"));
                    leaveTeamUseCase.execute(input);
                })
                .Then("his leave should be <result> because <note>", env -> {
                    var team = teamRepository.findById(env.gets("teamId")).get();
                    switch (env.getInput().get("result")){
                        case "allowed" -> {
                            assertFalse(team.getTeamMember(env.getInput().get("leave_user_id")).isPresent());
                        }
                        case "forbidden" -> {
                            assertTrue(team.getTeamMember(env.getInput().get("leave_user_id")).isPresent());
                        }
                        default -> {throw new RuntimeException("Unsupported status");}
                    }
                })
                .Execute();
    }
}
