package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.restore;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class RestoreBoardService implements RestoreBoardUseCase {

    private final TeamRepository teamRepository;
    public RestoreBoardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(RestoreBoardInput input) {

        Team team = teamRepository.findById(input.getTeamId()).get();
        team.restoreBoard(input.getBoardId());
        teamRepository.save(team);

        return CqrsOutput.create().setId(team.getId());
    }
}
