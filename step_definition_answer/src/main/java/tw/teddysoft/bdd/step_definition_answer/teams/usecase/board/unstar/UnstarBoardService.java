package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class UnstarBoardService implements UnstarBoardUseCase {

    private final TeamRepository teamRepository;
    public UnstarBoardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(UnstarBoardInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();
        team.unstarBoard(input.getBoardId());
        teamRepository.save(team);
        return CqrsOutput.create().setId(team.getId());
    }
}
