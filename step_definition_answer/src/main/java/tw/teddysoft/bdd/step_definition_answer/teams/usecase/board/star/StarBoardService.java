package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class StarBoardService implements StarBoardUseCase {

    private final TeamRepository teamRepository;
    public StarBoardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(StarBoardInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();
        team.starBoard(input.getBoardId());
        teamRepository.save(team);

        return CqrsOutput.create().setId(team.getId());
    }
}
