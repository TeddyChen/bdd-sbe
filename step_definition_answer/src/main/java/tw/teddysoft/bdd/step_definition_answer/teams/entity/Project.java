package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Project implements Entity<String> {
    private final String projectId;

    private String name;

    private final List<Board> boards;

    public Project(String projectId, String name) {
        this.projectId = projectId;
        this.name = name;
        boards = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return projectId;
    }

    public Optional<Board> getBoard(String boardId) {
        return boards.stream().filter(x -> x.getId().equals(boardId)).findAny();
    }

    public List<Board> getBoards() {
        return Collections.unmodifiableList(boards);
    }

    public void addBoard(Board board) {
        boards.add(board);
    }

    public void removeBoard(String boardId) {
        boards.removeIf( x -> x.getId().equals(boardId));
    }
}
