package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

public class TeamMemberDto {
    private String teamId;
    private String userId;
    private String role;

    public TeamMemberDto(String teamId, String userId, String role) {
        this.userId = userId;
        this.teamId = teamId;
        this.role = role;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
