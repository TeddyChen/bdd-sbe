package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;

public class TeamMapper {


    public static TeamDto toDto(Team team) {
        TeamDto teamDto = new TeamDto(team.getId(), team.getName());
        teamDto.setDefaultProject(ProjectMapper.toDto(team.getDefaultProject()));
        teamDto.setProjects(ProjectMapper.toDto(team.getUserDefinedProjects()));
        teamDto.setTrash(ProjectMapper.toDto(team.getTrash()));
        teamDto.setStarred(ProjectMapper.toDto(team.getStarred()));
        teamDto.setTeamMembers(TeamMemberMapper.toDto(team.getTeamMembers()));

        return teamDto;
    }

}
