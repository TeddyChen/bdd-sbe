package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite;

import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;

public class InviteTeamMemberService implements InviteTeamMemberUseCase {
    private final TeamRepository teamRepository;

    public InviteTeamMemberService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(InviteTeamMemberInput input) {

        var team = teamRepository.findById(input.getTeamId()).get();
        team.inviteTeamMember(input.getUserId(), input.getTeamRole());
        teamRepository.save(team);

        return CqrsOutput.create().setId(team.getId());
    }
}
