package tw.teddysoft.bdd.step_definition_answer.teams.entity;

public enum TeamRole {
    Admin, Staff
}
