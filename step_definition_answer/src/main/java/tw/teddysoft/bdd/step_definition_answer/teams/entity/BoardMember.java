package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.ValueObject;

import java.util.Objects;

public record BoardMember(String userId,
                          String boardId,
                          BoardRole boardRole) implements ValueObject {
    public BoardMember {
        Objects.requireNonNull(userId);
        Objects.requireNonNull(boardId);
        Objects.requireNonNull(boardRole);
    }
}
