package tw.teddysoft.bdd.step_definition_answer.teams.usecase;


import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.ezddd.core.usecase.Repository;


public interface TeamRepository extends Repository<Team, String> {
}
