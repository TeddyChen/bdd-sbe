package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class TrashBoardService implements TrashBoardUseCase {

    private final TeamRepository teamRepository;

    public TrashBoardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(TrashBoardInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();
        team.trashBoard(input.getBoardId());

        return CqrsOutput.create().setId(team.getId());
    }
}
