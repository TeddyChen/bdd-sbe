package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.*;

public class GetDashboardService implements GetDashboardUseCase {

    private final TeamRepository teamRepository;
    public GetDashboardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public GetDashboardOutput execute(GetDashboardInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();

        TeamDto teamDto = TeamMapper.toDto(team);
        for(ProjectDto projectDto: teamDto.getAllProjects()){
            for(BoardDto each : teamDto.getTrash().getBoards()){
                projectDto.getBoards().removeIf( x-> x.getName().equals(each.getName()));
            }
        }
        GetDashboardOutput output = new GetDashboardOutput();
        output.setTeamDto(teamDto);
        return output;
    }
}
