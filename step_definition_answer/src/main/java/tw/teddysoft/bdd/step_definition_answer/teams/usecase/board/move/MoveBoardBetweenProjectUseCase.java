package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface MoveBoardBetweenProjectUseCase extends UseCase<MoveBoardBetweenProjectInput, CqrsOutput> {
}
