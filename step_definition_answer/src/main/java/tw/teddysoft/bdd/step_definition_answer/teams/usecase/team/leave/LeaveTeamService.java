package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class LeaveTeamService implements LeaveTeamUseCase {

    private final TeamRepository teamRepository;
    public LeaveTeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(LeaveTeamInput input) {
            var team = teamRepository.findById(input.getTeamId()).get();
            team.leave(input.getUserId());
            teamRepository.save(team);
        return CqrsOutput.create().setId(team.getId());
    }
}
