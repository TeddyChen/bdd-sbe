package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface CreateTeamUseCase extends UseCase<CreateTeamInput, CqrsOutput> {
}
