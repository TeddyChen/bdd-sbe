package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard;

import tw.teddysoft.ezddd.cqrs.usecase.query.Query;

public interface GetDashboardUseCase extends Query<GetDashboardInput, GetDashboardOutput> {
}
