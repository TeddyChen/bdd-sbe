package tw.teddysoft.bdd.step_definition_answer.teams.entity;

public enum BoardRole {
    Admin, Staff
}
