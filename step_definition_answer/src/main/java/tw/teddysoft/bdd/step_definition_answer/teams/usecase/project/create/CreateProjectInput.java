package tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create;

import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateProjectInput implements Input {

    private String teamId;
    private String projectName;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
