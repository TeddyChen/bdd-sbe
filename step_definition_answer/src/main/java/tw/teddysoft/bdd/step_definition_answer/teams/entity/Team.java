package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.AggregateRoot;
import tw.teddysoft.ezddd.core.entity.DomainEvent;

import java.util.*;

public class Team extends AggregateRoot<String, DomainEvent> {

    public static final String DEFAULT_PROJECT_NAME = "$DEFAULT$";
    public static final String TRASH_PROJECT_NAME = "$TRASH$";
    public static final String STARRED_PROJECT_NAME = "$STARRED$";

    private final String teamId;
    private String name;
    private String userId;

    private final Project defaultProject;

    private final List<Project> userDefinedProjects;

    private final List<TeamMember> teamMembers;

    private final Project trash;

    private final Project starred;

    public Team(String teamId, String name, String userId) {
        this.teamId = teamId;
        this.name = name;
        this.userId = userId;
        teamMembers = new ArrayList<>();
        teamMembers.add(new TeamMember(userId, teamId, TeamRole.Admin));
        defaultProject = new Project(UUID.randomUUID().toString(), DEFAULT_PROJECT_NAME);
        userDefinedProjects = new ArrayList<>();
        trash = new Project(UUID.randomUUID().toString(), TRASH_PROJECT_NAME);
        starred = new Project(UUID.randomUUID().toString(), STARRED_PROJECT_NAME);
    }

    @Override
    public String getId() {
        return teamId;
    }

    public String getName() {
        return name;
    }

    public Optional<TeamMember> getTeamMember(String userId) {
        return teamMembers.stream().filter( x-> x.userId().equals(userId)).findAny();
    }

    public Project getDefaultProject() {
        return defaultProject;
    }

    public void createBoardInDefaultProject(String boardId, String boardName, String userId) {
        createBoardInProject(boardId, boardName, DEFAULT_PROJECT_NAME, userId);
    }

    public void createBoardInUserDefinedProject(String boardId, String boardName, String projectName, String userId) {
        createBoardInProject(boardId, boardName, projectName, userId);
    }

    private void createBoardInProject(String boardId, String boardName, String projectName, String userId){
        Objects.requireNonNull(boardId);
        Objects.requireNonNull(boardName);
        Objects.requireNonNull(projectName);
        Objects.requireNonNull(userId);

        if (getTeamMember(userId).isEmpty())
            throw new IllegalArgumentException("The user who is not a team member cannot create a board.");

        if (getTeamMember(userId).get().teamRole().equals(TeamRole.Staff))
            throw new IllegalArgumentException("The user who is a team staff cannot create a board. Only a team admin can.");

        if (DEFAULT_PROJECT_NAME.equals(projectName)) {
            defaultProject.addBoard(new Board(boardId, boardName, userId));
            return;
        }

        if (getUserDefinedProject(projectName).isEmpty()){
            throw new IllegalArgumentException("Project does not exist: " + projectName);
        }
        getUserDefinedProject(projectName).get().addBoard(new Board(boardId, boardName, userId));
    }

    public void inviteTeamMember(String userId, TeamRole teamRole) {
        teamMembers.add(new TeamMember(userId, getId(), teamRole));
    }

    public void leave(String leaveUserId) {
        if (isLastTeamAdmin())
            return;
        teamMembers.removeIf( x-> x.userId().equals(leaveUserId));
    }

    private boolean isLastTeamAdmin() {
        var adminCount = teamMembers.stream().filter( x -> x.teamRole().equals(TeamRole.Admin)).count();

        if (1 == teamMembers.stream().filter( x -> x.teamRole().equals(TeamRole.Admin)).count())
            return true;
        else return false;
    }

    public Optional<Project> getUserDefinedProject(String projectName) {
        return userDefinedProjects.stream().filter( x-> x.getName().equals(projectName)).findAny();
    }

    public void createProject(String projectName) {
        if (getUserDefinedProject(projectName).isPresent()){
            throw new IllegalArgumentException("Project name duplicated: " + projectName);
        }
        userDefinedProjects.add(new Project(projectName, projectName));
    }

    public void moveBoardBetweenProject(String toProjectName, String boardId) {
        Objects.requireNonNull(toProjectName);

        if (!getAllProjects().stream().anyMatch(x-> x.getName().equals(toProjectName))){
            throw new IllegalArgumentException("To project not found: " + toProjectName);
        }

        var optFromProject = getProjectFormBoardId(boardId);
        if (optFromProject.isEmpty()){
            throw new IllegalArgumentException("The board does not exit");
        }

        Project toProject = getAllProjects().stream().filter( x-> x.getName().equals(toProjectName)).findAny().get();
        toProject.addBoard(optFromProject.get().getBoard(boardId).get());
        optFromProject.get().removeBoard(boardId);
    }

    private List<Project> getAllProjects(){
        List<Project> result = new ArrayList<>(userDefinedProjects);
        result.add(defaultProject);
        return result;
    }

    private Optional<Project> getProjectFormBoardId(String boardId){
        for(Project project : getAllProjects()){
            for (Board board : project.getBoards()){
                if (board.getId().equals(boardId))
                    return Optional.of(project);
            }
        }
        return Optional.empty();
    }

    public List<Project> getUserDefinedProjects() {
        return Collections.unmodifiableList(userDefinedProjects);
    }

    public List<TeamMember> getTeamMembers() {
        return Collections.unmodifiableList(teamMembers);
    }

    public Project getTrash() {
        return trash;
    }

    public Project getStarred() {
        return starred;
    }

    public void trashBoard(String boardId) {
        var optBoard = getBoard(boardId);
        if (optBoard.isEmpty()){
            throw new IllegalArgumentException("Trashed board does not exist");
        }
        if (trash.getBoard(boardId).isPresent()){
            return;
        }

        trash.addBoard(optBoard.get());
    }

    public Optional<Board> getBoard(String boardId){
        for(Project project : getAllProjects()){
            if (project.getBoard(boardId).isPresent()){
                return project.getBoard(boardId);
            }
        }
        return Optional.empty();
    }

    public void restoreBoard(String boardId) {
        trash.removeBoard(boardId);
    }

    public void starBoard(String boardId) {
        var optBoard = getBoard(boardId);
        if (optBoard.isEmpty()){
            throw new IllegalArgumentException("Starred board does not exist");
        }
        if (starred.getBoard(boardId).isPresent()){
            return;
        }

        starred.addBoard(optBoard.get());
    }

    public void unstarBoard(String boardId) {
        starred.removeBoard(boardId);
    }
}
