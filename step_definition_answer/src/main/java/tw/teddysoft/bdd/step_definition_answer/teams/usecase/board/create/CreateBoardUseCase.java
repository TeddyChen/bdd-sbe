package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface CreateBoardUseCase extends UseCase<CreateBoardInput, CqrsOutput> {

}
