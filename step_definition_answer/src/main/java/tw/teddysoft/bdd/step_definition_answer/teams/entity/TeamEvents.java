package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.DomainEvent;

public interface TeamEvents extends DomainEvent {

    String teamId();
    default String aggregateId(){
        return teamId();
    }

}
