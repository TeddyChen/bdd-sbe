package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamRole;
import tw.teddysoft.ezddd.core.usecase.Input;

public class InviteTeamMemberInput implements Input {

    private String teamId;
    private String userId;
    private TeamRole teamRole;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TeamRole getTeamRole() {
        return teamRole;
    }

    public void setTeamRole(TeamRole teamRole) {
        this.teamRole = teamRole;
    }
}
