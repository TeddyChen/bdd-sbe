package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class CreateTeamService implements CreateTeamUseCase {

    private final TeamRepository repository;
    public CreateTeamService(TeamRepository teamRepository) {
        repository = teamRepository;
    }

    @Override
    public CqrsOutput execute(CreateTeamInput input) {
        Team team = new Team(
                input.getTeamId(),
                input.getTeamName(),
                input.getUserId());

        repository.save(team);

        return CqrsOutput.create().setId(team.getId());
    }
}
