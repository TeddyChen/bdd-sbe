package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

import java.util.ArrayList;
import java.util.List;

public class ProjectDto {

    private String projectId;
    private String name;
    private List<BoardDto> boards;

    public ProjectDto(String projectId, String name) {
        this.projectId = projectId;
        this.name = name;
        this.boards = new ArrayList<>();
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BoardDto> getBoards() {
        return boards;
    }

    public void setBoards(List<BoardDto> boards) {
        this.boards = boards;
    }

    public void addBoard(BoardDto boardDto) {
        this.boards.add(boardDto);
    }
}
