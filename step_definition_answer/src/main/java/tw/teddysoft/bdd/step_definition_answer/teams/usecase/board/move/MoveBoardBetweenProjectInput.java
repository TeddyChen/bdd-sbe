package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move;

import tw.teddysoft.ezddd.core.usecase.Input;

public class MoveBoardBetweenProjectInput implements Input {

    private String teamId;
    private String toProjectName;

    private String boardId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getToProjectName() {
        return toProjectName;
    }

    public void setToProjectName(String toProjectName) {
        this.toProjectName = toProjectName;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
}
