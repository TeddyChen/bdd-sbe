package tw.teddysoft.bdd.step_definition_answer.teams.usecase;


import tw.teddysoft.bdd.step_definition_answer.teams.entity.Board;
public class BoardMapper {
    public static BoardDto toDto(Board board) {
        return new BoardDto(board.getId(), board.getName());
    }
}
