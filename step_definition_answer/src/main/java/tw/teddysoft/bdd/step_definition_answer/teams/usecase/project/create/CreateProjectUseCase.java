package tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface CreateProjectUseCase extends UseCase<CreateProjectInput, CqrsOutput> {
}
