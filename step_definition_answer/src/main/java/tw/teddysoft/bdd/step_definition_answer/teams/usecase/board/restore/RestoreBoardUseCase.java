package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.restore;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface RestoreBoardUseCase extends UseCase<RestoreBoardInput, CqrsOutput> {
}
