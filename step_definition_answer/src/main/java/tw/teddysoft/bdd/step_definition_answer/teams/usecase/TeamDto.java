package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

import java.util.ArrayList;
import java.util.List;

public class TeamDto {

    private String teamId;
    private String teamName;
    private List<TeamMemberDto> teamMembers;

    private ProjectDto defaultProject;
    private List<ProjectDto> userDefinedProjects;
    private ProjectDto trash;
    private ProjectDto starred;

    public TeamDto() {

    }

    public TeamDto(String teamId, String teamName) {
        this();
        this.teamId = teamId;
        this.teamName = teamName;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<TeamMemberDto> getTeamMembers() { return teamMembers; }

    public void setTeamMembers(List<TeamMemberDto> teamMembers) { this.teamMembers = teamMembers; }

    public List<ProjectDto> getUserDefinedProjects() {
        return userDefinedProjects;
    }

    public void setProjects(List<ProjectDto> userDefinedProjects) {
        this.userDefinedProjects = userDefinedProjects;
    }

    public ProjectDto getTrash() {
        return trash;
    }

    public void setTrash(ProjectDto trash) {
        this.trash = trash;
    }

    public ProjectDto getStarred() {
        return starred;
    }

    public void setStarred(ProjectDto starred) {
        this.starred = starred;
    }

    public void setDefaultProject(ProjectDto defaultProject) {
        this.defaultProject = defaultProject;
    }

    public ProjectDto getDefaultProject(){
        return defaultProject;
    }

    public List<ProjectDto> getAllProjects(){
        List<ProjectDto> projectDtos = new ArrayList<>();
        projectDtos.add(defaultProject);
        projectDtos.addAll(userDefinedProjects);
        return projectDtos;
    }

}
