package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.unstar;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface UnstarBoardUseCase extends UseCase<UnstarBoardInput, CqrsOutput> {
}
