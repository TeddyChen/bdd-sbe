package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.star;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface StarBoardUseCase extends UseCase<StarBoardInput, CqrsOutput> {
}
