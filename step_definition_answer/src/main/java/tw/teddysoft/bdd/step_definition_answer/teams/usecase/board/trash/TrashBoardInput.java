package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash;

import tw.teddysoft.ezddd.core.usecase.Input;

public class TrashBoardInput implements Input {

    private String teamId;
    private String boardId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
}
