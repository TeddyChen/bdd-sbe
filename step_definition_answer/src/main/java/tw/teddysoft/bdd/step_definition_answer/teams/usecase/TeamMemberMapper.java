package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.TeamMember;

import java.util.ArrayList;
import java.util.List;

public class TeamMemberMapper {

    public static TeamMemberDto toDto(TeamMember teamMember) {
        return new TeamMemberDto(
                teamMember.teamId(),
                teamMember.userId(),
                teamMember.teamRole().name());
    }

    public static List<TeamMemberDto> toDto(List<TeamMember> teamMembers) {
        List<TeamMemberDto> teamMemberDto = new ArrayList<>();
        for(TeamMember member : teamMembers) {
            teamMemberDto.add(toDto(member));
        }
        return teamMemberDto;
    }

}

