package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.invite;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface InviteTeamMemberUseCase extends UseCase<InviteTeamMemberInput, CqrsOutput> {
}
