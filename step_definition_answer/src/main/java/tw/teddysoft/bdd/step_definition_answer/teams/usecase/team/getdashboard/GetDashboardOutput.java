package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamDto;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class GetDashboardOutput extends CqrsOutput {

    private TeamDto teamDto;

    public TeamDto getTeamDto() {
        return teamDto;
    }

    public void setTeamDto(TeamDto teamDto) {
        this.teamDto = teamDto;
    }
}
