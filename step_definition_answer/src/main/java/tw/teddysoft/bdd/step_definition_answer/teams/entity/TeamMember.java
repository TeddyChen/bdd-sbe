package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.ValueObject;

import java.util.Objects;

public record TeamMember(String userId,
                         String teamId,
                         TeamRole teamRole) implements ValueObject {
    public TeamMember {
        Objects.requireNonNull(userId);
        Objects.requireNonNull(teamId);
        Objects.requireNonNull(teamRole);
    }
}
