package tw.teddysoft.bdd.step_definition_answer.teams.entity;

import tw.teddysoft.ezddd.core.entity.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Board implements Entity<String> {
    private final String boardId;
    private String name;

    private List<BoardMember> boardMembers;

    public Board(String boardId, String name, String userId) {
        this.boardId = boardId;
        this.name = name;
        boardMembers = new ArrayList<>();
        addBoardMember(userId, BoardRole.Admin);
    }

    @Override
    public String getId() {
        return boardId;
    }

    public String getName() {
        return name;
    }

    public Optional<BoardMember> getBoardMember(String userId) {
        return boardMembers.stream().filter(x -> x.userId().equals(userId)).findAny();
    }

    public void addBoardMember(String userId, BoardRole boardRole) {
        this.boardMembers.add(new BoardMember(userId, boardId, boardRole));
    }
}
