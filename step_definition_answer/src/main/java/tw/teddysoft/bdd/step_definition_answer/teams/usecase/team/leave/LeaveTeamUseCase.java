package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface LeaveTeamUseCase extends UseCase<LeaveTeamInput, CqrsOutput> {
}
