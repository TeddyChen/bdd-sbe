package tw.teddysoft.bdd.step_definition_answer.teams.usecase;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Board;
import tw.teddysoft.bdd.step_definition_answer.teams.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectMapper {

    public static ProjectDto toDto(Project project) {
        ProjectDto projectDto = new ProjectDto(
                project.getId(),
                project.getName());

        for(Board board : project.getBoards()) {
            projectDto.addBoard(BoardMapper.toDto(board));
        }
        return projectDto;
    }

    public static List<ProjectDto> toDto(List<Project> projects) {
        List<ProjectDto> projectDtos = new ArrayList<>();
        for(Project project : projects) {
            projectDtos.add(toDto(project));
        }
        return projectDtos;
    }

}
