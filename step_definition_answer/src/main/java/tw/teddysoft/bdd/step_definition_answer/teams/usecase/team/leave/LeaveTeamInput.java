package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.leave;

import tw.teddysoft.ezddd.core.usecase.Input;

public class LeaveTeamInput implements Input {
    private String teamId;
    private String userId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
