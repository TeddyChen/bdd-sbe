package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.trash;

import tw.teddysoft.ezddd.core.usecase.UseCase;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public interface TrashBoardUseCase extends UseCase<TrashBoardInput, CqrsOutput> {
}
