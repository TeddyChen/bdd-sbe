package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.move;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class MoveBoardBetweenProjectService implements MoveBoardBetweenProjectUseCase {

    private final TeamRepository teamRepository;

    public MoveBoardBetweenProjectService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(MoveBoardBetweenProjectInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();
        team.moveBoardBetweenProject(input.getToProjectName(), input.getBoardId());
        teamRepository.save(team);
        return CqrsOutput.create().setId(team.getId());
    }
}
