package tw.teddysoft.bdd.step_definition_answer.teams.usecase.project.create;

import tw.teddysoft.bdd.step_definition_answer.teams.entity.Team;
import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;
import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

public class CreateProjectService implements CreateProjectUseCase {

    private final TeamRepository teamRepository;
    public CreateProjectService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(CreateProjectInput input) {
        Team team = teamRepository.findById(input.getTeamId()).get();
        team.createProject(input.getProjectName());
        teamRepository.save(team);
        return CqrsOutput.create().setId(team.getId());
    }
}
