package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.create;

import tw.teddysoft.ezddd.core.usecase.Input;

public class CreateTeamInput implements Input {
    private String teamId;
    private String teamName;
    private String userId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
