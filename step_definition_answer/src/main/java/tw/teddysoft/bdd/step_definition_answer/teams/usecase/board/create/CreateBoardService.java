package tw.teddysoft.bdd.step_definition_answer.teams.usecase.board.create;

import tw.teddysoft.ezddd.cqrs.usecase.CqrsOutput;

import tw.teddysoft.bdd.step_definition_answer.teams.usecase.TeamRepository;

public class CreateBoardService implements CreateBoardUseCase {

    private final TeamRepository teamRepository;

    public CreateBoardService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public CqrsOutput execute(CreateBoardInput input) {
        var team = teamRepository.findById(input.getTeamId()).get();
        if (null == input.getProjectName()){
            team.createBoardInDefaultProject(input.getBoardId(), input.getBoardName(), input.getUserId());
        }
        else {
            team.createBoardInUserDefinedProject(input.getBoardId(), input.getBoardName(), input.getProjectName(), input.getUserId());
        }
        teamRepository.save(team);

        return CqrsOutput.create().setId(input.getBoardId());
    }
}
