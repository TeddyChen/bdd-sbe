package tw.teddysoft.bdd.step_definition_answer.teams.usecase.team.getdashboard;

import tw.teddysoft.ezddd.core.usecase.Input;

public class GetDashboardInput implements Input {
    private String teamId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
